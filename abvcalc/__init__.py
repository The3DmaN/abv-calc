#Copyright (C) 2020 The3DmaN

#This file is part of ABV Calculator.

#ABV Calculator is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#ABV Calculator is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with ABV Calculator.  If not, see <http://www.gnu.org/licenses/>.

#Author: The3DmaN

import gi
import os

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class Handler:
	def onDestroy(self, *args):
		Gtk.main_quit()

	def brix_activate (self, switch, gparam):
		if switch.get_active():
			startlbl.set_text("Start Brix:")
			endlbl.set_text("Final Brix:")
			startsg.set_placeholder_text("22.9")
			finalsg.set_placeholder_text("8")
			startsg.set_text("")
			finalsg.set_text("")
			finalabv.set_text("")
		else:
			startlbl.set_text("Start SG:")
			endlbl.set_text("Final SG:")
			startsg.set_placeholder_text("1.096")
			finalsg.set_placeholder_text("1.032")
			startsg.set_text("")
			finalsg.set_text("")
			finalabv.set_text("")

	def calcbutton_click (self, widget):
		switchstate = brixswitch.get_state()
		if bool(switchstate)==False:
			startsgfloat = float(startsg.get_text())
			finalsgfloat = float(finalsg.get_text())
			startbrix = (((182.4601*startsgfloat-775.6821)*startsgfloat+1262.7794)*startsgfloat-669.5622)
			finalbrix = (((182.4601*finalsgfloat-775.6821)*finalsgfloat+1262.7794)*finalsgfloat-669.5622)
			correctfsg = 1.000-(0.004493*startbrix)+(0.011774*finalbrix)+(0.00027581*startbrix*startbrix)-(0.0012717*finalbrix*finalbrix)-(0.00000728*startbrix*startbrix*startbrix)+(0.000063293*finalbrix*finalbrix*finalbrix)
			abvround = str(round(((76.08*(startsgfloat-correctfsg)/(1.775-startsgfloat))*(correctfsg/0.794)* 100)/100, 2))
			finalabv.set_text(abvround + "%")

		else:
			startingbrix = float(startsg.get_text())
			finalbrix = float(finalsg.get_text())
			startbrixconv = (startingbrix / (258.6-((startingbrix / 258.2)*227.1))) + 1
			finalbrixconv = (finalbrix / (258.6-((finalbrix / 258.2)*227.1))) + 1
			startsgfloat = startbrixconv
			finalsgfloat = finalbrixconv
			correctfsg = 1.000-(0.004493*startingbrix)+(0.011774*finalbrix)+(0.00027581*startingbrix*startingbrix)-(0.0012717*finalbrix*finalbrix)-(0.00000728*startingbrix*startingbrix*startingbrix)+(0.000063293*finalbrix*finalbrix*finalbrix)
			abvround = str(round(((76.08*(startsgfloat-correctfsg)/(1.775-startsgfloat))*(correctfsg/0.794)* 100)/100, 2))
			finalabv.set_text(abvround + "%")


	def settings_clicked (self, widget):
		popmenu.popup()


builder = Gtk.Builder()
os.chdir(os.path.dirname(__file__))
builder.add_from_file("abv.glade")
builder.connect_signals(Handler())


window = builder.get_object("abvcalc")
startsg = builder.get_object("startsg")
startlbl = builder.get_object("startlbl")
finalsg = builder.get_object("finalsg")
endlbl = builder.get_object("endlbl")
finalabv = builder.get_object("finalabv")
settingsbar = builder.get_object("settingsbar")
menubutimg = builder.get_object("menubutimg")
brixswitch = builder.get_object("brixswitch")
popmenu = builder.get_object("popmenu")
window.show_all()


Gtk.main()
