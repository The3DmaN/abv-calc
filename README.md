# App Description:

ABV Calculator is a refractometer ABV calculator intended for beer or wine.
It is written in Python using GTK and Glade for the UI


ABV Calculator was designed to run on linux desktop and linux phones/tablets

# Install:

**Manjaro/Arch**
- ABV Calculator can be installed from AUR (https://aur.archlinux.org/packages/abv-calc-git/)

# Known issues

**Manjaro Plasma Mobile** - popover buttons do not work

Workaround: none

# Additional Info:

The mathematical formulas used in ABV Calculator are widely available, however, I first encountered these widely available formulas at:

Pete Drinks:
https://github.com/ahnlak/ABVCalc


**Warning: This calculator may not be super precise. Please use this calculator at your own risk, and verify your ABV with other sources before making any decisions. I take no responsibility for any decisions made from using this app.**

