#!/usr/bin/python3
import os
import setuptools
import setuptools.command.build_py
here = os.path.abspath(os.path.dirname(__file__))


class CreateDesktopFile(setuptools.command.build_py.build_py):
  def run(self):
    with open(os.path.join(here + "/abvcalc.desktop"), 'w') as f:
        f.write("[Desktop Entry]\n")
        f.write("Name=ABV Calculator\n")
        f.write("GenericName=ABV\n")
        f.write("Terminal=false\n")
        f.write("Type=Application\n")
        f.write("Categories=Application\n")
        f.write("Icon=abv\n")
        f.write("Exec=abv-calc-git\n")
        f.write("StartupWMClass=abv-calc-git\n")
        f.write("X-Purism-FormFactor=Workststion;Mobile;\n")
    setuptools.command.build_py.build_py.run(self)


# Workaround in case PyQt5 was installed without pip
install_requires=['requests']
install_requires.append("python-pyqt5")
install_requires.append("qt5-svg")
install_requires.append("git")

setuptools.setup(
    name='ABV Calculator',
    version='1.2',
    description='ABV Calculator for Refractometers',
    keywords='calculator',
    author='The3DmaN',
    url='https://gitlab.com/The3DmaN/abv-calc',
    install_requires=install_requires,
    python_requires='>=3.8',

    # From https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.8'
    ],
    cmdclass={'build_py': CreateDesktopFile},
    data_files=[
        ('share/icons/hicolor/scalable/apps', ['abvcalc/icons/abv.svg']),
        ('share/applications', ['abvcalc.desktop'])
    ],
    package_data={
        "": ["images/*.svg", "*.glade"]
    },
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts': [
            'abv-calc-git=abvcalc',
        ],
    },
)

